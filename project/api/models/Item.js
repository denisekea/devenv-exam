const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

const ItemSchema = new mongoose.Schema(
  {
    itemName: {
      type: String,
      required: [true, "can't be blank"],
    },
    completed: { type: Boolean },
  },
  { timestamps: true }
);

ItemSchema.plugin(uniqueValidator, { message: "already exists" });

const ItemModel = mongoose.model("Item", ItemSchema);

module.exports.ItemSchema = ItemSchema;
module.exports.ItemModel = ItemModel;

const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
const { ItemSchema } = require("./Item");

const ListSchema = new mongoose.Schema(
  {
    listName: {
      type: String,
      required: [true, "can't be blank"],
    },
    listItems: [ItemSchema],
  },
  { timestamps: true }
);

ListSchema.plugin(uniqueValidator, { message: "already exists" });

const ListModel = mongoose.model("List", ListSchema);
module.exports = ListModel;

const router = require("express").Router();
const UserModel = require("../models/User");

router.get("/getcurrent", async (req, res) => {
  const { userId } = req.session;

  if (userId) {
    const user = await UserModel.findOne({ _id: userId });
    return res.send({ user });
  }
  return res.send({ user: null });
});

module.exports = router;

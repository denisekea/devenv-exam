const express = require("express");
const mongoose = require("mongoose");
const path = require("path");
require("dotenv").config({ path: path.resolve(__dirname, "./.env") });
const session = require("express-session");
const rateLimiter = require("express-rate-limit");

const sessionRoute = require("./routes/session");
const authRoute = require("./routes/auth");
const listsRoute = require("./routes/todoLists");

const app = express();

// ------------ MIDDLEWARE ------------ //
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(
  session({
    name: process.env.SESSION_NAME,
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: { secure: false },
  })
);

app.use(
  rateLimiter({
    windowMs: 10 * 60 * 1000,
    max: 200,
  })
);

app.use(
  "/auth/",
  rateLimiter({
    windowMs: 10 * 60 * 1000, // 10 minutes
    max: 10,
  })
);

app.use("/session/", sessionRoute);
app.use("/auth", authRoute);
app.use("/lists", listsRoute);

// ------------ DB CONNECTION ------------ //

// mongodb atlas connection
mongoose.connect(process.env.MONGODB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false,
});

// ------------ RUN SERVER ------------ //

const port = process.env.PORT || 9000;

app.listen(port, () => {
  console.log("Server is running on port ", port);
});

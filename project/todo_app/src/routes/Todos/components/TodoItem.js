import React from "react";
import styled from "styled-components";
import {
  FiCircle as UncheckedIcon,
  FiCheckCircle as CheckedIcon,
  FiTrash as DeleteIcon,
} from "react-icons/fi";

export const ItemContainer = styled("div")`
  display: grid;
  grid-template-columns: 1.5em 1fr 1.5em;
  align-items: center;
  margin: 5px 0;

  .completedText {
    text-decoration: line-through;
  }

  svg {
    cursor: pointer;

    &:hover {
      fill: #69f6b7;
    }

    &.deleteIcon {
      fill: none;
      &:hover {
        stroke: red;
      }
    }
  }
`;

const TodoItem = (props) => {
  const { listId, item, updateListItems } = props;
  const { completed, itemName } = item;

  const handleCompletedStatus = async () => {
    const connection = await fetch(`/lists/${listId}/${item._id}`, {
      method: "PUT",
      body: JSON.stringify({ itemCompleted: !item.completed }),
      headers: { "Content-Type": "application/json" },
    });
    const data = await connection.json();

    if (data.listItems) {
      updateListItems(data.listItems);
    } else {
      console.log(data.error);
    }
  };

  const handleDeleteItem = async () => {
    const connection = await fetch(`/lists/${listId}/${item._id}`, {
      method: "PATCH",
    });
    const data = await connection.json();
    if (data.listItems) {
      updateListItems(data.listItems);
    } else {
      console.log(data.error);
    }
  };

  return (
    <ItemContainer>
      {completed ? (
        <CheckedIcon onClick={handleCompletedStatus} />
      ) : (
        <UncheckedIcon onClick={handleCompletedStatus} />
      )}
      <span className={completed ? "completedText" : ""}>{itemName}</span>
      <DeleteIcon className="deleteIcon" onClick={handleDeleteItem} />
    </ItemContainer>
  );
};

export default TodoItem;

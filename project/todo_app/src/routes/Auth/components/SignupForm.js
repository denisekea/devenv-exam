import React, { useState } from "react";
import { FormContainer, ErrorDisplay } from "./form.styled";

const SignupForm = ({ changeView, displayNotification }) => {
  const [errorMsg, setErrorMsg] = useState("");

  const handleSignUp = async (e) => {
    e.preventDefault();
    const form = document.querySelector("#signUpForm");
    const connection = await fetch("/auth/signup", {
      method: "POST",
      body: new FormData(form),
    });
    const data = await connection.json();
    if (data.user) {
      setErrorMsg("");
      displayNotification();
      setTimeout(() => changeView("login"), 2500);
    } else {
      setErrorMsg(data.error);
    }
  };

  return (
    <FormContainer id="signUpForm" method="POST" onSubmit={handleSignUp}>
      <label htmlFor="fullName">Full name</label>
      <input name="fullName" type="text" required />
      <label htmlFor="email">Email</label>
      <input name="email" type="email" required />
      <label htmlFor="password">Password</label>
      <input name="password" type="password" required />
      <label htmlFor="confirmPassword">Confirm password</label>
      <input name="confirmPassword" type="password" required />
      <button type="submit">Sign up</button>
      <ErrorDisplay>{errorMsg}</ErrorDisplay>
    </FormContainer>
  );
};

export default SignupForm;

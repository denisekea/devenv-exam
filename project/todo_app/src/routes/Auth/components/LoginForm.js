import React, { useState, useContext } from "react";
import AuthContext from "../../../common/AuthContext/AuthContext";
import { FormContainer, ErrorDisplay } from "./form.styled";

const LoginForm = ({ history }) => {
  const [errorMsg, setErrorMsg] = useState("");
  const { handleAuthChange } = useContext(AuthContext);

  const handleLogin = async (e) => {
    e.preventDefault();
    const form = document.querySelector("#loginForm");

    const connection = await fetch("/auth/login", {
      method: "POST",
      body: new FormData(form),
    });
    const data = await connection.json();

    if (data.user) {
      setErrorMsg("");
      handleAuthChange(data.user);
      history.push("/todos");
    } else {
      setErrorMsg(data.error);
    }
  };

  return (
    <FormContainer id="loginForm" method="POST" onSubmit={handleLogin}>
      <label htmlFor="email">Email</label>
      <input name="email" type="email" required />
      <label htmlFor="password">Password</label>
      <input name="password" type="password" required />
      <button type="submit">Log in</button>
      <ErrorDisplay>{errorMsg}</ErrorDisplay>
    </FormContainer>
  );
};

export default LoginForm;

import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { BrowserRouter as Router } from "react-router-dom";
import AuthContext from "../common/AuthContext/AuthContext";
import Loader from "../common/Loader/Loader";
import Routes from "../routes/Routes";

const AppContainer = styled("div")`
  position: relative;
  background: linear-gradient(
    346deg,
    rgba(255, 255, 255, 1) 0%,
    rgba(62, 0, 235, 1) 77%
  );
  height: 100%;
  margin: 0;
`;

const App = () => {
  const [loading, setLoading] = useState();
  const [auth, setAuth] = useState();

  const handleAuthChange = (authState) => {
    setAuth(authState);
  };

  const getSession = async () => {
    try {
      const res = await fetch("/session/getcurrent");
      const data = await res.json();

      if (data && data.user !== undefined) {
        await setAuth(data.user);
        setLoading(false);
      }
    } catch (e) {
      setAuth(false);
      setLoading(false);
      console.log(e);
    }
  };

  useEffect(() => {
    setLoading(true);
    getSession();
  }, []);

  return (
    <AuthContext.Provider value={{ auth, handleAuthChange }}>
      <Router basename="/devenv-exam">
        <AppContainer>{loading ? <Loader /> : <Routes />}</AppContainer>
      </Router>
    </AuthContext.Provider>
  );
};

export default App;

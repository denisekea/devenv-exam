import React from "react";
import styled, { keyframes } from "styled-components";
import CheckMarkIcon from "../assets/CheckMarkIcon";

const popupAnim = (top, height) => keyframes`
  20% {
    opacity: 1;
    height: ${height};
  }
  30% {
    top: ${top};
  }
  70% {
    opacity: 1;
  }
  85% {
    height: ${height};
    top: ${top};
  }
`;

const ConfirmationContainer = styled("div")`
  font-size: 1.1em;
  font-weight: bold;
  position: fixed;
  opacity: 0;
  top: 0;
  right: 42px;
  height: 0;
  width: auto;
  background-color: #69f6b7;
  border-radius: 5px;
  padding: 0.6em 1.1em 0;
  overflow: hidden;
  animation: ${popupAnim("1em", "2em")} 2.2s ease;
  z-index: 5000;

  svg {
    vertical-align: middle;
    margin-right: 10px;
    width: 26px;
    height: 26px;
  }
`;

const SavedConfirmation = ({ text }) => (
  <ConfirmationContainer>
    <CheckMarkIcon /> {text}
  </ConfirmationContainer>
);

export default SavedConfirmation;

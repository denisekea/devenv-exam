import React, { useContext } from "react";
import styled from "styled-components";
import { FiLogOut as LogOutIcon } from "react-icons/fi";
import AuthContext from "../AuthContext/AuthContext";

const NavContainer = styled("div")`
  position: relative;
  width: 300px;
  text-align: right;
  margin: 2em 3em 2em auto;
  vertical-align: middle;
  display: flex;
  justify-content: flex-end;

  div {
    display: inline-block;
    color: white;
    background-color: transparent;
    border-bottom: 2px solid white;
    font-weight: bold;
    font-size: 1.2em;
    margin: 0 1em;
    padding-bottom: 2px;
    cursor: pointer;

    &:hover {
      color: #69f6b7;
      border-color: #69f6b7;
    }
  }
`;

const LogOutButton = styled(LogOutIcon)`
  height: 20px;
  width: 20px;
  margin: 2px 1em 0;

  path,
  polyline,
  line {
    stroke: white;
    stroke-width: 3;
  }
  cursor: pointer;

  &:hover {
    path,
    polyline,
    line {
      stroke: #69f6b7;
    }
  }
`;

const NavBar = ({ history }) => {
  const { handleAuthChange } = useContext(AuthContext);

  const handleLogout = async () => {
    const connection = await fetch("/auth/logout");
    const data = await connection.json();
    if (data.error) {
      console.log("couldn't log out");
      return;
    }
    handleAuthChange(false);
    history.push("/logout");
  };

  return (
    <NavContainer>
      <div onClick={() => history.push("/todos")}>Lists</div>
      <div onClick={() => history.push("/profile")}>Profile</div>
      <LogOutButton onClick={handleLogout} />
    </NavContainer>
  );
};

export default NavBar;

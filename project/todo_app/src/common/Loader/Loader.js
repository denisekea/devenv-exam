import React from "react";
import styled from "styled-components";
import Loader from "react-loader-spinner";

const StyledLoader = styled(Loader)`
  position: absolute;
  top: 40%;
  left: 45%;
`;

const MainLoader = () => (
  <StyledLoader type="Grid" color="#69f6b7" height={120} width={120} />
);
export default MainLoader;

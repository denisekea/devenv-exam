module.exports = {
  env: {
    browser: true,
    es2020: true,
    jest: true
  },
  extends: ["plugin:react/recommended", "airbnb"],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 11,
    sourceType: "module",
  },
  plugins: ["react"],
  rules: {
    quotes: ["warn", "double"],
    indent: ["error", 2, { flatTernaryExpressions: true }],
    "linebreak-style": 0,
    "comma-dangle": 0,
    "operator-linebreak": [1, "before"],
    "no-underscore-dangle": 0,
    "no-static-element-interactions": 0,
    "no-confusing-arrow": 0,
    "newline-per-chained-call": ["error", { ignoreChainWithDepth: 2 }],
    "react/prop-types": 0,
    "react/jsx-filename-extension": 0,
    "react/jsx-one-expression-per-line": 0,
    "react/jsx-props-no-spreading": 0,
    "react/jsx-curly-newline": 0,
    "jsx-a11y/label-has-associated-control": 0,
    "implicit-arrow-linebreak": 0,
    "react/jsx-closing-bracket-location": 0,
    "jsx-a11y/click-events-have-key-events": 0,
    "jsx-a11y/no-static-element-interactions": 0,
    "react/jsx-curly-brace-presence": 0,
  },
};

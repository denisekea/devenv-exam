# DevEnv mandatory 2 / exam

## Deployed pages

Frontend: [https://denisekea.gitlab.io/devenv-exam/](https://denisekea.gitlab.io/devenv-exam/)

Documentation: [https://denisekea.gitlab.io/devenv-exam-docs/](https://denisekea.gitlab.io/devenv-exam-docs/)

## Run documentation - Gatsby

cd docs

yarn / npm install

yarn start / npm start

yarn build / npm build

yarn run lint / npm run lint

yarn run format / npm run format

## Run backend - Node/Express

cd project/api

yarn / npm install

node app.js

## Run frontend - React

cd project/todo_app

yarn / npm install

yarn start / npm start

---
title: 'Project introduction'
metaTitle: 'Project introduction'
metaDescription: 'Introduction to the shared list app'
---

This documentation website is intended as a reference document to all developers in the team and ensures our new team members are off to a good start.

## The project: a shared list app

This is a simple shared list app that allows you to share your lists with your team or household.

![Shared list project](./project-gif.gif 'Project animation')

It allows you to:

- create lists
- delete lists
- create list items
- mark list items completed or incomplete
- delete list items
- sign up as a user
- log in and log out
- delete your user account

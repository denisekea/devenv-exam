---
title: 'Git'
metaTitle: 'Git usage'
metaDescription: 'More about how we use git in our team.'
---

## Branches

For branch names we require a prefix feat or fix for features or bug fixes, so it’s easy to spot what is being worked on in that branch.

- New feature: **feat/new-feature-name**
- Bug fix: **fix/bug-description**

## Commits

- Commit messages should always start with a verb that describes the change. E.g. _“added config file”_ or _“fixed a login form bug"_.
- We should avoid piling lots of changes into 1 commit and instead make a habit of logically separating changes into specific commits.

## Merging

**1. Rebase the new local branch before pushing**<br />
Before a branch will be pushed remotely and merged to master, we should always rebase a branch on the latest version of master locally with git rebase.

`git rebase -i master`

Rebase places all the new changes of the local branch on top of the latest version of master and allows you to solve conflicts, before you even push your branch or attempt to merge your branch to master.

Find a good explanation of how git rebase works here: <br />
https://www.atlassian.com/git/tutorials/rewriting-history/git-rebase

**2. Push the rebased branch and create a merge request** <br />
Wait for all the CI/CD jobs to finish and pass after you've pushed. Only create a merge request when all the checks have passed.

**3. Code review and approval**<br />
After you have created a merge request, it needs to be reviewed and approved by another developer to ensure that bugs are caught before ending up in production.

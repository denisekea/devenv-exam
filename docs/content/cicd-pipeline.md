---
title: 'CI/CD'
metaTitle: 'CI/CD - Continuous Integration and Continuous Delivery'
metaDescription: 'Description of the CI/CD process'
---

## Continuous Integration and Continuous Delivery

Our team values a culture of CI/CD in order to ensure that our product changes reach the end user quickly, safely and reliably.

We set up a CI/CD pipeline to automatically run jobs when branches are pushed, for example linting and testing. Merging to the master branch also automatically triggers deployment.

## Stages

- lint
- build
- test
- deploy

## YML file

If you want to add something to the CI/CD pipeline you can edit the `gitlab-ci.yml` file in the root of the repository.

```yml
image: node:latest

stages:
  - lint
  - build
  - test
  - deploy

before_script:
  - cd project/todo_app
  - yarn

cache:
  paths:
    - project/todo_app/node_modules/

eslint_frontend:
  stage: lint
  script:
    - yarn run lint
  only:
    changes:
      - 'project/todo_app/**/*'

build_frontend:
  stage: build
  script:
    - yarn run build
  only:
    changes:
      - 'project/todo_app/**/*'

test_frontend:
  stage: test
  script:
    - yarn run test
  only:
    changes:
      - 'project/todo_app/**/*'

pages:
  stage: deploy
  script:
    - yarn run build
    - rm -rf ../../public
    - mv build ../../public
  artifacts:
    paths:
      - public
  only:
    - master
```

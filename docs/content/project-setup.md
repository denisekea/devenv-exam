---
title: 'Project set-up'
metaTitle: 'Project set-up'
metaDescription: 'This is the meta description for this page'
---

## Get started

### Clone the repository

```
git clone https://gitlab.com/denisekea/devenv-exam.git

or

git clone git@gitlab.com:denisekea/devenv-exam.git
```

### Repository structure

```
repo
│
└───docs
│
└───project
│   └───api
│   └───todo_app

```

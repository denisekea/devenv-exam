---
title: 'IDE'
metaTitle: 'Integrated Development Environments'
metaDescription: 'This is the meta description for this page'
---


## Config VSCode

Use Visual Studio Code as IDE and install all necessary extensions.

**EsLint by Dirk Baeumer**
The extension uses the ESLint library installed in the opened workspace folder. If the folder doesn't provide one, the extension looks for a global install version.


